# CC0 Images

This repository contains images released as Public Domain, through [CC0][].
Yup, **that** [CC0][].


[CC0]: https://creativecommons.org/publicdomain/zero/1.0/
